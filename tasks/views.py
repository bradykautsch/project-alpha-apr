from django.shortcuts import render, redirect
from tasks.forms import CreateTaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.assignee = request.user
            task.save()
            return redirect("list_projects")
    else:
        form = CreateTaskForm()

    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    tasks_list = Task.objects.filter(assignee=request.user)
    context = {
        "tasks_list": tasks_list,
    }
    return render(request, "tasks/show_my_tasks.html", context)
